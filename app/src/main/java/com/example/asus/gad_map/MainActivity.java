package com.example.asus.gad_map;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.amap.api.maps.AMap;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.MyLocationStyle;
import com.amap.api.services.busline.BusStationQuery;
import com.amap.api.services.busline.BusStationResult;
import com.amap.api.services.busline.BusStationSearch;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends Activity implements View.OnClickListener,BusStationSearch.OnBusStationSearchListener {
    private MapView mMapView = null;
    private AMap aMap;
    private Button btnInDoor,weixinMap,nightMap,lukuangMap,languageMap,screenMap;

    public static int index_laguage;
    private static int index_lukuang;
    private static int index_night;
    private static int index_weixin;

    //公交车数据
    private BusStationQuery busStationQuery;
    private BusStationSearch busStationSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        离线地图
//        startActivity(new Intent(this.getApplicationContext(),
//                com.amap.api.maps.offlinemap.OfflineMapActivity.class));

        mMapView = findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);// 此方法须覆写，虚拟机需要在很多情况下保存地图绘制的当前状态。
//初始化地图控制器对象

        if (aMap == null) {
            aMap = mMapView.getMap();
        }

        initBlueLocation();

        btnInDoor = findViewById(R.id.indoor_map);
        weixinMap = findViewById(R.id.weixing_map);
        nightMap = findViewById(R.id.night_map);
        lukuangMap = findViewById(R.id.lukuang_map);
        languageMap = findViewById(R.id.language_map);
        screenMap = findViewById(R.id.screen_map);

        btnInDoor.setOnClickListener(this);
        weixinMap.setOnClickListener(this);
        nightMap.setOnClickListener(this);
        lukuangMap.setOnClickListener(this);
        languageMap.setOnClickListener(this);
        screenMap.setOnClickListener(this);

        index_lukuang = 0;
        index_laguage = 0;
        index_night = 0;
        index_weixin = 0;

        //查询公交
        busStationQuery = new BusStationQuery("83","020");

        busStationSearch = new BusStationSearch(this, busStationQuery);
        busStationSearch.setOnBusStationSearchListener(this);// 设置查询结果的监听

        busStationSearch.searchBusStationAsyn();

    }

    private void initBlueLocation() {
        MyLocationStyle myLocationStyle;
        myLocationStyle = new MyLocationStyle();//初始化定位蓝点样式类myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_LOCATION_ROTATE);//连续定位、且将视角移动到地图中心点，定位点依照设备方向旋转，并且会跟随设备移动。（1秒1次定位）如果不设置myLocationType，默认也会执行此种模式。
        myLocationStyle.interval(2000); //设置连续定位模式下的定位间隔，只在连续定位模式下生效，单次定位模式下不会生效。单位为毫秒。
        aMap.setMyLocationStyle(myLocationStyle);//设置定位蓝点的Style
        //aMap.getUiSettings().setMyLocationButtonEnabled(true);设置默认定位按钮是否显示，非必需设置。
        aMap.setMyLocationEnabled(true);// 设置为true表示启动显示定位蓝点，false表示隐藏定位蓝点并不进行定位，默认是false。
        myLocationStyle.showMyLocation(true);
        myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_MAP_ROTATE_NO_CENTER);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //在activity执行onDestroy时执行mMapView.onDestroy()，销毁地图
        mMapView.onDestroy();
    }
    @Override
    protected void onResume() {
        super.onResume();
        //在activity执行onResume时执行mMapView.onResume ()，重新绘制加载地图
        mMapView.onResume();
    }
    @Override
    protected void onPause() {
        super.onPause();
        //在activity执行onPause时执行mMapView.onPause ()，暂停地图的绘制
        mMapView.onPause();
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //在activity执行onSaveInstanceState时执行mMapView.onSaveInstanceState (outState)，保存地图当前的状态
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.indoor_map:
                aMap.showIndoorMap(true);
                Toast.makeText(this, "已开启室内地图", Toast.LENGTH_SHORT).show();
                break;

            case R.id.weixing_map:
                index_weixin++;
                if (index_weixin%2!=0){
                    aMap.setMapType(AMap.MAP_TYPE_SATELLITE);// 设置卫星地图模式，aMap是地图控制器对象。
                    weixinMap.setText("标准地图");
                    Toast.makeText(this, "已开启卫星地图", Toast.LENGTH_SHORT).show();
                }else {
                    aMap.setMapType(AMap.MAP_TYPE_NORMAL);// 设置卫星地图模式，aMap是地图控制器对象。
                    weixinMap.setText("卫星地图");
                    Toast.makeText(this, "已开启标准地图", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.night_map:
                index_night++;
                if (index_night%2!=0){
                    aMap.setMapType(AMap.MAP_TYPE_NIGHT);// 设置卫星地图模式，aMap是地图控制器对象。
                    nightMap.setText("白昼地图");
                    Toast.makeText(this, "已开启夜间地图", Toast.LENGTH_SHORT).show();
                }else {
                    aMap.setMapType(AMap.MAP_TYPE_NORMAL);// 设置卫星地图模式，aMap是地图控制器对象。
                    nightMap.setText("夜间地图");
                    Toast.makeText(this, "已开启白昼地图", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.lukuang_map:
                index_lukuang++;
                if (index_lukuang%2!=0){
                    aMap.setTrafficEnabled(true);//显示实时路况图层，aMap是地图控制器对象。
                    lukuangMap.setText("路况:关");
                    Toast.makeText(this, "已开启路况信息", Toast.LENGTH_SHORT).show();
                }else {
                    aMap.setTrafficEnabled(false);//显示实时路况图层，aMap是地图控制器对象。
                    lukuangMap.setText("路况:开");
                    Toast.makeText(this, "已关闭路况信息", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.language_map:
                index_laguage++;
                if (index_laguage%2!=0){
                    Toast.makeText(this, "start english map", Toast.LENGTH_SHORT).show();
                    languageMap.setText("中文地图");
                    aMap.setMapLanguage(AMap.ENGLISH);
                }else {
                    Toast.makeText(this, "开启中文地图", Toast.LENGTH_SHORT).show();
                    languageMap.setText("english map");
                    aMap.setMapLanguage(AMap.CHINESE);
                }
                break;

            case R.id.screen_map:
                screenMapPhoto();
                break;
        }
    }

    //截屏
    private void screenMapPhoto() {
        /**
         * 对地图进行截屏
         */
        aMap.getMapScreenShot(new AMap.OnMapScreenShotListener() {
            @Override
            public void onMapScreenShot(Bitmap bitmap) {

            }

            @Override
            public void onMapScreenShot(Bitmap bitmap, int status) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                if(null == bitmap){
                    return;
                }
                try {
                    FileOutputStream fos = new FileOutputStream(
                            Environment.getExternalStorageDirectory() + "/test_"
                                    + sdf.format(new Date()) + ".png");
                    boolean b = bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                    try {
                        fos.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    StringBuffer buffer = new StringBuffer();
                    if (b)
                        buffer.append("截屏成功 ");
                    else {
                        buffer.append("截屏失败 ");
                    }
                    if (status != 0)
                        buffer.append("地图渲染完成，截屏无网格");
                    else {
                        buffer.append( "地图未渲染完成，截屏有网格");
                    }
                    Toast.makeText(MainActivity.this, buffer.toString(), Toast.LENGTH_SHORT).show();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

            }
        });
    }


    @Override
    public void onBusStationSearched(BusStationResult busStationResult, int i) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int j=0;j<busStationResult.getBusStations().size();j++){
            stringBuffer.append(" ").append(busStationResult.getBusStations().get(j));
        }
        Toast.makeText(this, "该车的信息:"+stringBuffer.toString()+i, Toast.LENGTH_SHORT).show();
    }
}